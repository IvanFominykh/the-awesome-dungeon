<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="The Awesome dungeon" FOLDED="false" ID="ID_1744811735" CREATED="1541493126444" MODIFIED="1542472933593" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="0.75">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_icon_for_attributes="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="19" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="&#x411;&#x43e;&#x43d;&#x443;&#x441;&#x44b;" POSITION="right" ID="ID_1065887852" CREATED="1541493153303" MODIFIED="1542472944657">
<edge COLOR="#ff0000"/>
<node TEXT="&#x41e;&#x442;&#x43a;&#x440;&#x44b;&#x442;&#x44b;&#x435;" ID="ID_1874446473" CREATED="1542472265514" MODIFIED="1542472965434"/>
<node TEXT="&#x421;&#x43a;&#x440;&#x44b;&#x442;&#x44b;&#x435;" ID="ID_1511503649" CREATED="1541494135271" MODIFIED="1542472974056">
<node TEXT="&#x412;&#x43e;&#x437;&#x43c;&#x43e;&#x436;&#x43d;&#x44b; &#x43a;&#x430;&#x43a; &#x431;&#x430;&#x444;&#x44b;, &#x442;&#x430;&#x43a; &#x438; &#x434;&#x435;&#x431;&#x430;&#x444;&#x44b;" ID="ID_1520406890" CREATED="1542472976116" MODIFIED="1542472998648"/>
<node TEXT="&#x422;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; &#x431;&#x430;&#x444;&#x44b;" ID="ID_1634251535" CREATED="1542473043136" MODIFIED="1542473102977"/>
</node>
</node>
<node TEXT="&#x418;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441;" POSITION="right" ID="ID_12550328" CREATED="1541493197262" MODIFIED="1542472201307">
<edge COLOR="#00007c"/>
</node>
<node TEXT="&#x421;&#x442;&#x440;&#x443;&#x43a;&#x442;&#x443;&#x440;&#x430; &#x438;&#x433;&#x440;&#x44b;" POSITION="right" ID="ID_317336123" CREATED="1541493177937" MODIFIED="1542472368133">
<edge COLOR="#0000ff"/>
<node TEXT="&#x41d;&#x435;&#x441;&#x43a;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; &#x443;&#x440;&#x43e;&#x432;&#x43d;&#x435;&#x439;" ID="ID_965979948" CREATED="1542472328070" MODIFIED="1542472409927"/>
<node TEXT="&#x41e;&#x434;&#x43d;&#x430; &#x431;&#x43e;&#x43b;&#x44c;&#x448;&#x430;&#x44f; &#x43a;&#x430;&#x440;&#x442;&#x430;" ID="ID_942677785" CREATED="1542472321659" MODIFIED="1542472398096">
<font BOLD="false" ITALIC="false"/>
</node>
</node>
<node TEXT="&#x414;&#x438;&#x43d;&#x430;&#x43c;&#x438;&#x447;&#x43d;&#x43e;&#x441;&#x442;&#x44c;" POSITION="right" ID="ID_1587757401" CREATED="1541493187588" MODIFIED="1542472169355">
<edge COLOR="#00ff00"/>
<node TEXT="&#x41f;&#x43e;&#x448;&#x430;&#x433;&#x43e;&#x432;&#x430;&#x44f;" ID="ID_652553411" CREATED="1542472441740" MODIFIED="1542472449255"/>
<node TEXT="&#x412; &#x440;&#x435;&#x430;&#x43b;&#x44c;&#x43d;&#x43e;&#x43c; &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x438;" ID="ID_260560504" CREATED="1541493646454" MODIFIED="1542472461551"/>
</node>
<node TEXT="&#x41f;&#x43e;&#x434;&#x441;&#x447;&#x435;&#x442; &#x43e;&#x447;&#x43a;&#x43e;&#x432;" POSITION="left" ID="ID_1183770888" CREATED="1542472792300" MODIFIED="1542472892900">
<edge COLOR="#007c00"/>
<node TEXT="&#x417;&#x430;&#x432;&#x438;&#x441;&#x438;&#x442; &#x43e;&#x442; &#x432;&#x440;&#x435;&#x43c;&#x435;&#x43d;&#x438;, &#x432;&#x44b;&#x43f;&#x43e;&#x43b;&#x43d;&#x435;&#x43d;&#x438;&#x44f; &#x434;&#x43e;&#x441;&#x442;&#x438;&#x436;&#x435;&#x43d;&#x438;&#x439; &#x438; &#x442;.&#x434;." ID="ID_874142439" CREATED="1542472845985" MODIFIED="1542472874138">
<node TEXT="&#x421;&#x440;&#x430;&#x432;&#x43d;&#x435;&#x43d;&#x438;&#x435; &#x440;&#x435;&#x437;&#x443;&#x43b;&#x44c;&#x442;&#x430;&#x442;&#x430;" ID="ID_1245537899" CREATED="1542472481327" MODIFIED="1542472835474">
<node TEXT="&#x421; &#x441;&#x430;&#x43c;&#x438;&#x43c; &#x441;&#x43e;&#x431;&#x43e;&#x439;" ID="ID_521529524" CREATED="1542472693542" MODIFIED="1542472704112"/>
<node TEXT="&#x421;&#x43e; &#x432;&#x441;&#x435;&#x43c;&#x438; &#x438;&#x433;&#x440;&#x43e;&#x43a;&#x430;&#x43c;&#x438;" ID="ID_315771543" CREATED="1542472686011" MODIFIED="1542472720484">
<node TEXT="&#x422;&#x430;&#x431;&#x43b;&#x438;&#x446;&#x430; &#x43b;&#x438;&#x434;&#x435;&#x440;&#x43e;&#x432;" ID="ID_1984329231" CREATED="1542472725311" MODIFIED="1542472740476"/>
</node>
<node TEXT="&#x41d;&#x435;&#x442; &#x441;&#x440;&#x430;&#x432;&#x43d;&#x435;&#x43d;&#x438;&#x44f;" ID="ID_137041922" CREATED="1542473060801" MODIFIED="1542473066625"/>
</node>
</node>
<node TEXT="&#x417;&#x430;&#x432;&#x438;&#x441;&#x438;&#x442; &#x442;&#x43e;&#x43b;&#x44c;&#x43a;&#x43e; &#x43e;&#x442; &#x43f;&#x43e;&#x431;&#x435;&#x434;&#x44b;/&#x43f;&#x43e;&#x440;&#x430;&#x436;&#x435;&#x43d;&#x438;&#x44f;" ID="ID_860655134" CREATED="1542472818983" MODIFIED="1542472925962"/>
</node>
</node>
</map>
