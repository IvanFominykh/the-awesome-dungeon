import cocos
import pyglet
import random
import level2
import time
from level2 import *
from pygame import mixer
import pyglet.app
from pyglet import font
from pyglet.window import key as keys
from math import *
from random import choice
from cocos.cocosnode import *
from cocos.director import director
from cocos.layer import *
from cocos.sprite import *
from cocos.actions import *
from cocos.text  import *
from cocos.scene import Scene
from cocos.menu import *
from cocos.director import director
from cocos.scenes.transitions import *

Game_status=True
VICTORY=False
COLOR = (0, 139, 139)
Endgame_label = 'Awesome'



mixer.init()
s_you_died1 = mixer.Sound("sound/terrible.wav")
s_entry  = mixer.Sound("sound/entry.wav")
s_entry2  = mixer.Sound("sound/entry2.wav")
s_entry3  = mixer.Sound("sound/entry3.wav")
s_destroyed  = mixer.Sound("sound/destroyed.wav")
s_victory = mixer.Sound('sound/victory.wav')
s_startgame = mixer.Sound('sound/startgame.wav')
s_bigger_greater = mixer.Sound('sound/bigger_greater.wav')
s_blow = mixer.Sound('sound/a_powerful_blow.wav')
s_terrible = mixer.Sound('sound/terrible.wav')
s_another_one = mixer.Sound('sound/another_one.wav')


class OptionsMenu( Menu ):
    def __init__(self):
        super( OptionsMenu, self).__init__('the-awesome-dungeon') 

        self.font_title['font_name'] = 'UA_A_Charming'
        self.font_title['font_size'] = 100
        self.font_title['color'] = (204,164,164,255)
        self.font_item['font_name'] = 'UA_A_Charming',
        self.font_item['color'] = (32,48,32,255)
        self.font_item['font_size'] = 60
        self.font_item_selected['font_name'] = 'UA_A_Charming'
        self.font_item_selected['color'] = (139,0,0,255)
        self.font_item_selected['font_size'] = 60

        self.menu_anchor_y = CENTER
        self.menu_anchor_x = CENTER

        items = []

        
        items.append( ToggleMenuItem('Show FPS:', self.on_show_fps, director.show_FPS) )
        items.append( MenuItem('Fullscreen', self.on_fullscreen) )
        items.append( MenuItem('Back', self.on_quit) )
        self.create_menu( items, shake(), shake_back() )

    def on_fullscreen( self ):
        director.window.set_fullscreen( not director.window.fullscreen )

    def on_quit( self ):
        self.parent.switch_to(0)

    def on_show_fps( self, value ):
        director.show_FPS = value

class MainMenu( Menu ):
   

    def __init__(self):
        super( MainMenu, self).__init__('the-awesome-dungeon') 
        self.do(CallFunc(choice([s_entry, s_entry2,s_entry3]).play))
        self.font_title['font_name'] = 'UA_A_Charming'
        self.font_title['font_size'] = 100
        self.font_title['color'] = (204,164,164,255)
        self.font_item['font_name'] = 'UA_A_Charming',
        self.font_item['color'] = (32,48,32,255)
        self.font_item['font_size'] = 60
        self.font_item_selected['font_name'] = 'UA_A_Charming'
        self.font_item_selected['font_size'] = 60
        self.menu_anchor_y = CENTER
        self.menu_anchor_x = CENTER
        items = []
        self.font_item_selected['color'] = (139,0,0,255)
        items.append( MenuItem('New Game', self.on_new_game) )
        items.append( MenuItem('Options', self.on_options) )
        items.append( MenuItem('Quit', self.on_quit) )
        self.create_menu( items, shake(), shake_back() )

    def on_new_game(self):
        global Game_status
        if Game_status==False:
            Game_status=True
        F=Level1()
        director.push(FadeTransition(F, duration=2))
        s_entry.stop()
        s_entry2.stop()
        s_entry3.stop()
        s_startgame.play()

    def on_options( self ):
        self.parent.switch_to(1)
        
    def on_quit(self):
        pyglet.app.exit()
        
class HUD(Layer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.score = 260
        self.label = Label(str(self.score), position=(725,25), font_name="Times New Roman", font_size=32, color=(0,0,0,255))
        self.add(self.label)

class PlayerInput(Layer):
    is_event_handler = True
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keyable = True
    def allow_movement(self):
        self.keyable = True
    
        
    def on_key_press(self, key, modifiers):
        global Game_status
        global Endgame_label
        global Victory
#you
        if self.keyable:
            if key==keys.W and self.parent.a.you.y<356:
                self.keyable = False
                self.parent.a.you.do(MoveBy((0,21), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                            +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x, \
                                              self.parent.a.you.y+21), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x, colorcell.y))
            if key==keys.A and self.parent.a.you.x>292:
                self.keyable = False
                self.parent.a.you.do(MoveBy((-21,0), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                     +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x-21, \
                                              self.parent.a.you.y), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x, colorcell.y))         
            if key==keys.S and self.parent.a.you.y>170:
                self.keyable = False
                self.parent.a.you.do(MoveBy((0,-21), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                     +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x, \
                                              self.parent.a.you.y-21), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x+1-1, colorcell.y+1-1))    
            if key==keys.D and self.parent.a.you.x<490:
                self.keyable = False
                self.parent.a.you.do(MoveBy((21,0), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                     +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x+21, \
                                              self.parent.a.you.y), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x, colorcell.y))

#smth 
            if key==keys.W or key==keys.A or key==keys.S or key==keys.D or key==keys.H:
                self.parent.h.score -= 1
                if self.parent.h.score==0:
                    Game_status=False
                if self.parent.h.score>120:
                    Endgame_label = 'Awesome'
                elif self.parent.h.score>=70:
                    Endgame_label = 'Well fought'    
                elif self.parent.h.score>0:
                    Endgame_label = 'Enough to win'
                self.parent.h.label.element.text = str(self.parent.h.score)
#chiort
                if self.parent.a.chiort.x!=301 and self.parent.k==0:
                    self.parent.a.chiort.do(MoveBy((-21,0), 0.005))
                else:
                    self.parent.a.chiort.scale_x=-1
                    if self.parent.t!=0:
                        self.parent.a.chiort.do(MoveBy((21,0), 0.005))
                    if self.parent.a.chiort.x!=469:
                        self.parent.k=1
                        self.parent.t=1
                    else:
                        self.parent.a.chiort.scale_x=1
                        self.parent.k=0
                        self.parent.t=0             
#orc
                orcs_to_die = []
                for orc in self.parent.a.list_orc:
                    if orc.y<308 and orc.x==332:     
                        orc.do(MoveBy((0,21), 0.005))
                    if orc.x<=458 and orc.y==328:
                        orc.do(MoveBy((21,0), 0.005))
                    if orc.x==458 and orc.y>=201:
                        orc.do(MoveBy((0,-21), 0.005))
                    if orc.x>=340 and orc.y==202:
                        orc.do(MoveBy((-21,0), 0.005))
                    if (orc.x==self.parent.a.you.x and orc.y-21==self.parent.a.you.y and key==keys.W) or \
                       (orc.x+21==self.parent.a.you.x and orc.y==self.parent.a.you.y and key==keys.A) or \
                       (orc.x==self.parent.a.you.x and orc.y+21==self.parent.a.you.y and key==keys.S) or \
                       (orc.x-21==self.parent.a.you.x and orc.y==self.parent.a.you.y and key==keys.D):
                        orcs_to_die.append(orc)
                        black = cocos.sprite.Sprite('pic/cell.png', position=(orc.x, orc.y), color=(0,0,0))
                        self.parent.a.Cell_layer.add(black)
                        orc.do(CallFunc(orc.kill))
                        self.parent.a.dead_orc_set.add((orc.x, orc.y))
                        
                    for dead_orc in orcs_to_die:
                        if (dead_orc in self.parent.a.list_orc)==True:
                            self.do(CallFunc(random.choice([s_destroyed, s_blow, s_another_one]).play))
                            self.parent.a.list_orc.remove(dead_orc)

                if self.parent.a.dead_orc_set.issubset(self.parent.big_set_changed):
                    self.parent.big_set_changed-=self.parent.a.dead_orc_set
                    self.parent.a.dead_orc_set.clear()
                elif self.parent.a.dead_orc_set.isdisjoint(self.parent.big_set_changed):
                    self.parent.a.dead_orc_set.clear()

#victory 
            if self.parent.big_set_changed==self.parent.big_set and self.parent.a.dead_orc_set==set():
                s_victory.play()
                global VICTORY
                VICTORY = True

class Act(Layer):
    def update(self, dt):
        global Game_status
        global VICTORY
#chiort
        if ((self.chiort.y+10==349) or (self.chiort.y+10==370)) and \
            ((self.you.y==349) or (self.you.y==370)) and Game_status==True:
            if (self.chiort.x+10==self.you.x) or \
                (self.chiort.x-11==self.you.x) or \
                (self.chiort.x+10==self.you.x) or \
                (self.chiort.x-11==self.you.x):
                    self.you.do(CallFunc(self.you.kill))
                    Game_status=False


#orc
        for orc in self.parent.a.list_orc:
            if (orc.x==self.parent.a.you.x and orc.y==self.parent.a.you.y):
                self.orcs_to_die.append(orc)
                black = cocos.sprite.Sprite('pic/cell.png', position=(orc.x, orc.y), color=(0,0,0))
                self.Cell_layer.add(black)
                orc.do(CallFunc(orc.kill))
                self.dead_orc_set.add((orc.x, orc.y))
                for dead_orc in self.orcs_to_die:
                    if (dead_orc in self.list_orc)==True:
                        self.do(CallFunc(random.choice([s_destroyed, s_blow, s_another_one]).play))
                        self.list_orc.remove(dead_orc)
                        
        if Game_status==True and VICTORY==True:
            if self.cycle==0:
                self.cycle=1
                self.Endgame_layer = cocos.layer.Layer()
                self.add(self.Endgame_layer)
                self.letter2 = Label(text=Endgame_label, position=(170,280), font_size=150, color=(85, 190, 80,255), font_name = 'UA_A_Charming') 
                self.add(self.letter2)
            else:
                time.sleep(4)
                Lvl2 = Level2()
                director.push(Lvl2)
                
        if Game_status==False and VICTORY==False:
            if self.cycle==0:
                self.cycle=1
                s_terrible.play()
                self.Endgame_layer = cocos.layer.Layer()
                self.add(self.Endgame_layer)
                self.letter2 = Label(text='You lose', position=(250,275), font_size=150, color=(133, 8, 12, 255), font_name = 'UA_A_Charming') 
                self.add(self.letter2)
            else:
                time.sleep(4)
                scene2 = Scene()
                scene2.add( MultiplexLayer(
                                MainMenu(), 
                                OptionsMenu(),
                                ),
                            z=1 )
                director.push(scene2)
                
                
    def __init__(self, coord, coord1, coord2, coord3, chiortxy):
        super().__init__()
        if Game_status==True:
            self.schedule(self.update)
        
        self.cycle = 0    
        self.list_orc=[]
        self.chiort_set= set()
        self.coord_set= set()
        self.orcs_to_die = []
        self.dead_orc_set = set()
        
        self.Cell_layer=cocos.layer.Layer()
        self.add(self.Cell_layer)
        self.orc = cocos.sprite.Sprite('pic/orc.png', position=coord, opacity=255, scale=1.1)
        self.add(self.orc)
        self.list_orc.append(self.orc)
        self.orc1 = cocos.sprite.Sprite('pic/orc.png', position=coord1, opacity=255, scale=1.1)
        self.add(self.orc1)
        self.list_orc.append(self.orc1)
        self.orc2 = cocos.sprite.Sprite('pic/orc.png', position=coord2, opacity=255, scale=1.1)
        self.add(self.orc2)
        self.list_orc.append(self.orc2)
        self.orc3 = cocos.sprite.Sprite('pic/orc.png', position=coord3, opacity=255, scale=1.1)
        self.add(self.orc3)
        self.list_orc.append(self.orc3)
        self.chiort = cocos.sprite.Sprite('pic/chiort.png', position=chiortxy, opacity=255, scale=1.1)
        self.add(self.chiort)
        self.you = cocos.sprite.Sprite('pic/knight.png', position=(395, 265), opacity=255, scale=1/10)
        self.add(self.you)
        

        

        



        

        
class Level1(cocos.scene.Scene):
    def __init__(self):
        super().__init__()
        Background = cocos.layer.util_layers.ColorLayer(240, 220, 130, 255, width=800, height=600)
        self.add(Background)
        self.big_set=set()
        self.big_set_changed=set()
        self.list=[]
        self.list1=[]
        self.chiort_list=[]
        self.k=0
        self.t=0
        self.b=0

        for i in range(11):
            for j in range(11):
                cell = cocos.sprite.Sprite('pic/cell.png', position=(290+j*21,160+i*21), opacity=255)
                Background.add(cell)
                for k in range(7):
                    if k<=i<11-k and k<=j<11-k:
                        cell.color=(185+k*10,185+k*10,185+k*10)
                if i==5 and j==5:
                    cell.color=COLOR
                    self.big_set_changed.add((cell.x, cell.y))
                if ((i==2 or i==8) and 2<j<8 and j%2==0) or ((i==2 or j==8) and 2<i<8 and i%2==0):
                    self.list.append((cell.x,cell.y))
                    
                elif i==10 and j>3:
                    self.chiort_list.append((cell.x-10,cell.y-10))
                self.big_set.add((cell.x, cell.y))
                
        self.l = PlayerInput()
        self.add(self.l)
        self.h=HUD()
        self.add(self.h)
        coord=random.choice(self.list)
        self.list.remove(coord)
        coord1=random.choice(self.list)
        self.list.remove(coord1)
        coord2=random.choice(self.list)
        self.list.remove(coord2)
        coord3=random.choice(self.list)
        self.list.remove(coord3)
        chiortxy=random.choice(self.chiort_list)
        self.a = Act(coord, coord1, coord2, coord3, chiortxy)
        self.add(self.a)
        L=Label(text='the-awesome-dungeon', font_name = 'UA_A_Charming', position=(180,490), font_size=100, align='right', color=(0,0,0,255)) 
        L1=Label(text='level 1', font_name = 'UA_A_Charming', position=(365,405), font_size=70, align='right', color=(0,0,0,255)) 
        Background.add(L) 
        Background.add(L1)



     
if __name__ == '__main__':
    font.add_directory('fonts')
    cocos.director.director.init(caption='the-awesome-dungeon', width=800, height=600)
    scene = Scene()
    scene.add( MultiplexLayer(
                    MainMenu(), 
                    OptionsMenu(),
                    ),
                z=1 )

    director.run( scene )
    
mixer.quit()
