import cocos
import pyglet
import random
from level3 import *
from gamestart import *

COLOR = (255, 140, 0)
Game_status = True
VICTORY = False
Endgame_label = 'Awesome'

class HUD(Layer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.score = 180
        self.label = Label(str(self.score), position=(725,25), font_name="Times New Roman", font_size=32, color=(0,0,0,255))
        self.add(self.label)

class PlayerInput(Layer):
    is_event_handler = True
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keyable = True
    def allow_movement(self):
        self.keyable = True
    
        
    def on_key_press(self, key, modifiers):
        global Game_status
#you
        if self.keyable:
            if key==keys.W and self.parent.a.you.y<356:
                self.keyable = False
                self.parent.a.you.do(MoveBy((0,21), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                            +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x, \
                                              self.parent.a.you.y+21), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x, colorcell.y))
            if key==keys.A and self.parent.a.you.x>292:
                self.keyable = False
                self.parent.a.you.do(MoveBy((-21,0), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                     +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x-21, \
                                              self.parent.a.you.y), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x, colorcell.y))         
            if key==keys.S and self.parent.a.you.y>170:
                self.keyable = False
                self.parent.a.you.do(MoveBy((0,-21), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                     +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x, \
                                              self.parent.a.you.y-21), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x+1-1, colorcell.y+1-1))    
            if key==keys.D and self.parent.a.you.x<490:
                self.keyable = False
                self.parent.a.you.do(MoveBy((21,0), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                     +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x+21, \
                                              self.parent.a.you.y), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x, colorcell.y))

#smth 
            if key==keys.W or key==keys.A or key==keys.S or key==keys.D or key==keys.H:
                self.parent.h.score -= 1
                if self.parent.h.score==0:
                    Game_status=False
                if self.parent.h.score>50:
                    Endgame_label = 'Awesome'
                elif self.parent.h.score>=30:
                    Endgame_label = 'Well fought'    
                elif self.parent.h.score>0:
                    Endgame_label = 'Enough to win'
                self.parent.h.label.element.text = str(self.parent.h.score)
#chiort
                if self.parent.a.chiort1.x!=301 and self.parent.k1==0:
                    self.parent.a.chiort1.do(MoveBy((-21,0), 0.005))
                else:
                    self.parent.a.chiort1.scale_x=-1
                    if self.parent.t1!=0:
                        self.parent.a.chiort1.do(MoveBy((21,0), 0.005))
                    if self.parent.a.chiort1.x!=469:
                        self.parent.k1=1
                        self.parent.t1=1
                    else:
                        self.parent.a.chiort1.scale_x=1
                        self.parent.k1=0
                        self.parent.t1=0

                if self.parent.a.chiort2.x!=301 and self.parent.k2==0:
                    self.parent.a.chiort2.do(MoveBy((-21,0), 0.005))
                else:
                    self.parent.a.chiort2.scale_x=-1
                    if self.parent.t2!=0:
                        self.parent.a.chiort2.do(MoveBy((21,0), 0.005))
                    if self.parent.a.chiort2.x!=469:
                        self.parent.k2=1
                        self.parent.t2=1
                    else:
                        self.parent.a.chiort2.scale_x=1
                        self.parent.k2=0
                        self.parent.t2=0 
#orc
                orcs_to_die = []
                for orc in self.parent.a.list_orc:
                    if orc.y<308 and orc.x==332:     
                        orc.do(MoveBy((0,21), 0.005))
                    if orc.x<=458 and orc.y==328:
                        orc.do(MoveBy((21,0), 0.005))
                    if orc.x==458 and orc.y>=201:
                        orc.do(MoveBy((0,-21), 0.005))
                    if orc.x>=340 and orc.y==202:
                        orc.do(MoveBy((-21,0), 0.005))
                    if (orc.x==self.parent.a.you.x and orc.y-21==self.parent.a.you.y and key==keys.W) or \
                       (orc.x+21==self.parent.a.you.x and orc.y==self.parent.a.you.y and key==keys.A) or \
                       (orc.x==self.parent.a.you.x and orc.y+21==self.parent.a.you.y and key==keys.S) or \
                       (orc.x-21==self.parent.a.you.x and orc.y==self.parent.a.you.y and key==keys.D):
                        orcs_to_die.append(orc)
                        black = cocos.sprite.Sprite('pic/cell.png', position=(orc.x, orc.y), color=(0,0,0))
                        self.parent.a.Cell_layer.add(black)
                        orc.do(CallFunc(orc.kill))
                        self.parent.a.dead_orc_set.add((orc.x, orc.y))
                        
                    for dead_orc in orcs_to_die:
                        if (dead_orc in self.parent.a.list_orc)==True:
                            self.do(CallFunc(random.choice([s_destroyed, s_blow, s_another_one]).play))
                            self.parent.a.list_orc.remove(dead_orc)

                if self.parent.a.dead_orc_set.issubset(self.parent.big_set_changed):
                    self.parent.big_set_changed-=self.parent.a.dead_orc_set
                    self.parent.a.dead_orc_set.clear()
                elif self.parent.a.dead_orc_set.isdisjoint(self.parent.big_set_changed):
                    self.parent.a.dead_orc_set.clear()

#scull
                scull_remove = []
                for scull123 in self.parent.a.list_scull:
                    scull123.do(MoveBy((42,0), 0.003))
                    if scull123.x>499:
                        scull123.opacity=0
                    if scull123.x>520:
                        scull123.do(MoveBy((-252,0), 0.003))
                        scull123.opacity=255
                    if ((scull123.y==self.parent.a.you.y and scull123.x+21==self.parent.a.you.x) or\
                       (scull123.y==self.parent.a.you.y and scull123.x+42==self.parent.a.you.x)) and\
                       key==keys.A:
                        self.parent.a.you.do(CallFunc(self.parent.a.you.kill))
                        Game_status=False
               

#victory 
            if self.parent.big_set_changed==self.parent.big_set and self.parent.a.dead_orc_set==set():
                s_victory.play()
                global VICTORY
                VICTORY = True

class Act(Layer):
    def update(self, dt):
        global Game_status
        global VICTORY

#chiort
        if ((self.chiort1.y+10==349) or (self.chiort1.y+10==370)) and \
            ((self.you.y==349) or (self.you.y==370)) and Game_status==True:
            if (self.chiort1.x+10==self.you.x) or \
                (self.chiort1.x-11==self.you.x) or \
                (self.chiort1.x+10==self.you.x) or \
                (self.chiort1.x-11==self.you.x):
                    self.you.do(CallFunc(self.you.kill))
                    Game_status=False

        if ((self.chiort2.y+10==181) or (self.chiort2.y+10==160)) and \
            ((self.you.y==181) or (self.you.y==160)) and Game_status==True:
            if (self.chiort2.x+10==self.you.x) or \
                (self.chiort2.x-11==self.you.x) or \
                (self.chiort2.x+10==self.you.x) or \
                (self.chiort2.x-11==self.you.x):
                    self.you.do(CallFunc(self.you.kill))
                    Game_status=False

#scull
        if (self.scull.y==self.you.y and self.scull.x==self.you.x) and Game_status==True:
            self.you.do(CallFunc(self.you.kill))
            Game_status=False
 
#orc
        for orc in self.parent.a.list_orc:
            if (orc.x==self.you.x and orc.y==self.you.y) or \
               (orc.x==self.scull.x and orc.y==self.scull.y):
                self.orcs_to_die.append(orc)
                black = cocos.sprite.Sprite('pic/cell.png', position=(orc.x, orc.y), color=(0,0,0))
                self.Cell_layer.add(black)
                orc.do(CallFunc(orc.kill))
                self.dead_orc_set.add((orc.x, orc.y))
                for dead_orc in self.orcs_to_die:
                    if (dead_orc in self.list_orc)==True:
                        self.do(CallFunc(random.choice([s_destroyed, s_blow, s_another_one]).play))
                        self.list_orc.remove(dead_orc)
                        
        if Game_status==True and VICTORY==True:
            if self.cycle==0:
                self.cycle=1
                self.Endgame_layer = cocos.layer.Layer()
                self.add(self.Endgame_layer)
                self.letter2 = Label(text=Endgame_label, position=(170,280), font_size=150, color=(222, 184, 135,255), font_name = 'UA_A_Charming')
                self.add(self.letter2)
            else:
                time.sleep(4)
                Lvl3 = Level3()
                director.push(Lvl3)
                 
        if Game_status==False and VICTORY==False:
            if self.cycle==0:
                self.cycle=1
                s_terrible.play()
                self.Endgame_layer = cocos.layer.Layer()
                self.add(self.Endgame_layer)
                self.letter2 = Label(text='You lose', position=(250,275), font_size=150, color=(133, 8, 12, 255), font_name = 'UA_A_Charming')
                self.add(self.letter2)
            else:
                time.sleep(4)
                scene1 = Scene()
                scene1.add( MultiplexLayer(
                                MainMenu(), 
                                OptionsMenu(),
                                ),
                            z=1 )
                director.push(scene1)
                
                
    def __init__(self, coord, coord1, coord2, coord3, chiortxy1, chiortxy2, scullxy):
        super().__init__()
        if Game_status==True:
            self.schedule(self.update)
        
        self.cycle = 0    
        self.list_orc=[]
        self.chiort_set1= set()
        self.chiort_set2= set()
        self.coord_set= set()
        self.orcs_to_die = []
        self.dead_orc_set = set()
        self.list_scull=[]
        
        self.Cell_layer=cocos.layer.Layer()
        self.add(self.Cell_layer)
        self.orc = cocos.sprite.Sprite('pic/orc.png', position=coord, opacity=255, scale=1.1)
        self.add(self.orc)
        self.list_orc.append(self.orc)
        self.orc1 = cocos.sprite.Sprite('pic/orc.png', position=coord1, opacity=255, scale=1.1)
        self.add(self.orc1)
        self.list_orc.append(self.orc1)
        self.orc2 = cocos.sprite.Sprite('pic/orc.png', position=coord2, opacity=255, scale=1.1)
        self.add(self.orc2)
        self.list_orc.append(self.orc2)
        self.orc3 = cocos.sprite.Sprite('pic/orc.png', position=coord3, opacity=255, scale=1.1)
        self.add(self.orc3)
        self.list_orc.append(self.orc3)
        self.chiort1 = cocos.sprite.Sprite('pic/chiort.png', position=chiortxy1, opacity=255, scale=1.1)
        self.add(self.chiort1)
        self.chiort2 = cocos.sprite.Sprite('pic/chiort.png', position=chiortxy2, opacity=255, scale=1.1)
        self.add(self.chiort2)
        self.you = cocos.sprite.Sprite('pic/knight.png', position=(395, 265), opacity=255, scale=1/10)
        self.add(self.you)
        self.scull = cocos.sprite.Sprite('pic/scull.png', position=scullxy, opacity=255, scale=1)
        self.add(self.scull)
        self.list_scull.append(self.scull)
        

        

        



        

        
class Level2(cocos.scene.Scene):
    def __init__(self):
        super().__init__()
        Background = cocos.layer.util_layers.ColorLayer(107, 142, 35, 255, width=800, height=600)
        self.add(Background)
        self.big_set=set()
        self.big_set_changed=set()
        self.list=[]
        self.list1=[]
        self.chiort_list1=[]
        self.chiort_list2=[]
        self.scull_list=[]
        self.k1=0
        self.t1=0
        self.k2=0
        self.t2=0
        self.b=0
        randcoord=random.choice([3,5,7])
        for i in range(11):
            for j in range(11):
                cell = cocos.sprite.Sprite('pic/cell.png', position=(290+j*21,160+i*21), opacity=255)
                Background.add(cell)
                for k in range(7):
                    if k<=i<11-k and k<=j<11-k:
                        cell.color=(200+k*13,180+k*15,180+k*15)
                if i==5 and j==5:
                    cell.color=COLOR
                    self.big_set_changed.add((cell.x, cell.y))
                if ((i==2 or i==8) and 2<j<8 and j%2==0) or ((i==2 or j==8) and 2<i<8 and i%2==0):
                    self.list.append((cell.x,cell.y))
                elif i==10 and j>3:
                    self.chiort_list1.append((cell.x-10,cell.y-10))           
                elif i==1 and j>3:
                    self.chiort_list2.append((cell.x-10,cell.y-10))
                self.big_set.add((cell.x, cell.y))
                if i==randcoord and j==0:
                    self.scull_list.append((cell.x,cell.y))
                
        self.l = PlayerInput()
        self.add(self.l)
        self.h=HUD()
        self.add(self.h)
        coord=random.choice(self.list)
        self.list.remove(coord)
        coord1=random.choice(self.list)
        self.list.remove(coord1)
        coord2=random.choice(self.list)
        self.list.remove(coord2)
        coord3=random.choice(self.list)
        self.list.remove(coord3)
        chiortxy1=random.choice(self.chiort_list1)
        chiortxy2=random.choice(self.chiort_list2)
        scullxy=random.choice(self.scull_list)
        self.a = Act(coord, coord1, coord2, coord3, chiortxy1, chiortxy2, scullxy)
        self.add(self.a)
        L=Label(text='the-awesome-dungeon', font_name = 'UA_A_Charming', position=(180,490), font_size=100, align='right', color=(0,0,0,255)) 
        L1=Label(text='level 2', font_name = 'UA_A_Charming', position=(365,405), font_size=70, align='right', color=(0,0,0,255)) 
        Background.add(L) 
        Background.add(L1)
