import cocos
import pyglet
import random
from level2 import *
from gamestart import *

COLOR = (189, 183, 107)
Game_status = True
VICTORY = False
Endgame_layer = ''

class HUD(Layer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.score = 175
        self.label = Label(str(self.score), position=(725,25), font_name="Times New Roman", font_size=32, color=(0,0,0,255))
        self.add(self.label)

class PlayerInput(Layer):
    is_event_handler = True
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keyable = True
    def allow_movement(self):
        self.keyable = True
    
        
    def on_key_press(self, key, modifiers):
        global Game_status
#you
        if self.keyable:
            if key==keys.W and self.parent.a.you.y<356:
                self.keyable = False
                self.parent.a.you.do(MoveBy((0,21), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                            +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x, \
                                              self.parent.a.you.y+21), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x, colorcell.y))
            if key==keys.A and self.parent.a.you.x>292:
                self.keyable = False
                self.parent.a.you.do(MoveBy((-21,0), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                     +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x-21, \
                                              self.parent.a.you.y), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x, colorcell.y))         
            if key==keys.S and self.parent.a.you.y>170:
                self.keyable = False
                self.parent.a.you.do(MoveBy((0,-21), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                     +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x, \
                                              self.parent.a.you.y-21), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x+1-1, colorcell.y+1-1))    
            if key==keys.D and self.parent.a.you.x<490:
                self.keyable = False
                self.parent.a.you.do(MoveBy((21,0), 0.005)+ScaleBy(1.2, 0.05)+ScaleBy(10/12, 0.05)\
                                     +CallFunc(self.allow_movement))
                colorcell=cocos.sprite.Sprite('pic/cell.png', position=(self.parent.a.you.x+21, \
                                              self.parent.a.you.y), color=COLOR)
                self.parent.a.Cell_layer.add(colorcell)
                self.parent.big_set_changed.add((colorcell.x, colorcell.y))

#smth 
            if key==keys.W or key==keys.A or key==keys.S or key==keys.D or key==keys.H:
                self.parent.h.score -= 1
                if self.parent.h.score==0:
                    Game_status=False
                if self.parent.h.score>50:
                    Endgame_label = 'Awesome'
                elif self.parent.h.score>=20:
                    Endgame_label = 'Well fought'    
                elif self.parent.h.score>0:
                    Endgame_label = 'Enough to win'
                self.parent.h.label.element.text = str(self.parent.h.score)
#chiort


                if self.parent.a.chiort2.x!=301 and self.parent.k2==0:
                    self.parent.a.chiort2.do(MoveBy((-21,0), 0.005))
                else:
                    self.parent.a.chiort2.scale_x=-1
                    if self.parent.t2!=0:
                        self.parent.a.chiort2.do(MoveBy((21,0), 0.005))
                    if self.parent.a.chiort2.x!=469:
                        self.parent.k2=1
                        self.parent.t2=1
                    else:
                        self.parent.a.chiort2.scale_x=1
                        self.parent.k2=0
                        self.parent.t2=0 


#scull
                
                scull_remove = []
                for scull123 in self.parent.a.list_scull:
                    scull123.do(MoveBy((42,0), 0.003))
                    if scull123.x>499:
                        scull123.opacity=0
                    if scull123.x>520:
                        scull123.do(MoveBy((-252,0), 0.003))
                        scull123.opacity=255
                    if ((scull123.y==self.parent.a.you.y and scull123.x+21==self.parent.a.you.x) or\
                       (scull123.y==self.parent.a.you.y and scull123.x+42==self.parent.a.you.x)) and\
                       key==keys.A:
                        self.parent.a.you.do(CallFunc(self.parent.a.you.kill))
                        Game_status=False
                        
#frog
                for froggo in self.parent.a.list_frog:        
                    if froggo.x!=290 and self.parent.frog_const1==0:
                        froggo.do(MoveBy((-21,0), 0.005))
                        black = cocos.sprite.Sprite('pic/cell.png', position=(froggo.x-21, froggo.y), color=(0,0,0))
                        self.parent.a.Cell_layer.add(black)
                    else:
                        froggo.scale_x=-1
                        if self.parent.frog_const2!=0:
                            froggo.do(MoveBy((21,0), 0.005))
                            black = cocos.sprite.Sprite('pic/cell.png', position=(froggo.x+21, froggo.y), color=(0,0,0))
                            self.parent.a.Cell_layer.add(black)
                        if froggo.x!=479:
                            self.parent.frog_const1=1
                            self.parent.frog_const2=1
                        else:
                            froggo.scale_x=1
                            self.parent.frog_const1=0
                            self.parent.frog_const2=0
                    
                    if (froggo.x+21==self.parent.a.you.x and froggo.y==self.parent.a.you.y and\
                        key==keys.A and self.parent.frog_const1==1) or\
                        (froggo.x-21==self.parent.a.you.x and froggo.y==self.parent.a.you.y and\
                        key==keys.D and self.parent.frog_const1==0):
                        froggo.do(CallFunc(froggo.kill))
                        self.parent.a.frogs_to_die.append(froggo)
                    for dead_froggo in self.parent.a.frogs_to_die:
                        if (dead_froggo in self.parent.a.list_frog)==True:
                            self.do(CallFunc(random.choice([s_destroyed, s_blow, s_another_one]).play))
                            self.parent.a.list_frog.remove(dead_froggo)
                            self.parent.a.froggo_set.add((froggo.x, froggo.y))

                if self.parent.a.froggo_set.issubset(self.parent.big_set_changed):
                    self.parent.big_set_changed-=self.parent.a.froggo_set
                    self.parent.a.froggo_set.clear()
                elif self.parent.a.froggo_set.isdisjoint(self.parent.big_set_changed):
                    self.parent.a.froggo_set.clear()                

#victory 
            if self.parent.big_set_changed==self.parent.big_set and self.parent.a.froggo_set==set():
                s_victory.play()
                global VICTORY
                VICTORY = True

class Act(Layer):
    def update(self, dt):
        global Game_status
        global VICTORY
#chiort

        if ((self.chiort2.y+10==181) or (self.chiort2.y+10==160)) and \
            ((self.you.y==181) or (self.you.y==160)) and Game_status==True:
            if (self.chiort2.x+10==self.you.x) or \
                (self.chiort2.x-11==self.you.x) or \
                (self.chiort2.x+10==self.you.x) or \
                (self.chiort2.x-11==self.you.x):
                    self.you.do(CallFunc(self.you.kill))
                    Game_status=False

#scull
        if ((self.scull1.y==self.you.y and self.scull1.x==self.you.x) or \
           (self.scull2.y==self.you.y and self.scull2.x==self.you.x) or \
            (self.scull3.y==self.you.y and self.scull3.x==self.you.x)) and Game_status==True:
            self.you.do(CallFunc(self.you.kill))
            Game_status=False
#frog
        for froggo in self.list_frog:
            if froggo.x==self.you.x and froggo.y==self.you.y:
                froggo.do(CallFunc(froggo.kill))
                self.frogs_to_die.append(froggo)
            for dead_froggo in self.frogs_to_die:
                if (dead_froggo in self.list_frog)==True:
                    self.do(CallFunc(random.choice([s_destroyed, s_blow, s_another_one]).play))
                    self.parent.a.list_frog.remove(dead_froggo)
                    self.froggo_set.add((froggo.x, froggo.y))


#GAME                        
                        
        if Game_status==True and VICTORY==True:
            if self.cycle==0:
                self.cycle=1
                self.Endgame_layer = cocos.layer.Layer()
                self.add(self.Endgame_layer)
                self.letter2 = Label(text=Endgame_label, position=(170,280), font_size=150, color=(173, 255, 47,255), font_name = 'UA_A_Charming')
                self.add(self.letter2)
            else:
                time.sleep(1)
                scene_end = Scene()
                End_layer = Layer()
                end_text1 = Label(text='Great!', position=(285,400), font_size=80, color=(0, 100, 0, 255), font_name = 'Times New Roman')
                end_text2 = Label(text='Your work is done', position=(170,300), font_size=50, color=(0, 100, 0, 255), font_name = 'Times New Roman')
                end_text3 = Label(text='Creator: Ivan Fominykh', position=(535,100), font_size=18, color=(46, 139, 87, 255), font_name = 'Times New Roman')
                end_text4 = Label(text='Mentor: Fedor Lyanguzov', position=(535,50), font_size=18, color=(46, 139, 87, 255), font_name = 'Times New Roman')
                end_text5 = Label(text='My email: Ivan-fom@inbox.ru', position=(35,50), font_size=18, color=(46, 139, 87, 255), font_name = 'Times New Roman')
                End_layer.add(end_text1)
                End_layer.add(end_text2)
                End_layer.add(end_text3)
                End_layer.add(end_text4)
                End_layer.add(end_text5)
                scene_end.add(End_layer)
                director.run(scene_end)
                time.sleep(6)
      
        if Game_status==False and VICTORY==False:
            if self.cycle==0:
                self.cycle=1
                s_terrible.play()
                self.Endgame_layer = cocos.layer.Layer()
                self.add(self.Endgame_layer)
                self.letter2 = Label(text='You lose', position=(250,275), font_size=150, color=(133, 8, 12, 255), font_name = 'UA_A_Charming')
                self.Endgame_layer.add(self.letter2)
            else:
                time.sleep(4)
                scene1 = Scene()
                scene1.add( MultiplexLayer(
                                MainMenu(), 
                                OptionsMenu(),
                                ),
                            z=1 )
                director.push(scene1)
                
                
    def __init__(self, chiortxy2, scullxy1, scullxy2, scullxy3, frogxy):
        super().__init__()
        if Game_status==True:
            self.schedule(self.update)
        
        self.cycle = 0    
        self.chiort_set2= set()
        self.coord_set= set()
        self.list_scull=[]
        self.list_frog=[]
        self.frogs_to_die = []
        self.froggo_set= set()
        
        self.Cell_layer=cocos.layer.Layer()
        self.add(self.Cell_layer)
        self.chiort2 = cocos.sprite.Sprite('pic/chiort.png', position=chiortxy2, opacity=255, scale=1.1)
        self.add(self.chiort2)
        self.you = cocos.sprite.Sprite('pic/knight.png', position=(395, 265), opacity=255, scale=1/10)
        self.add(self.you)
        self.scull1 = cocos.sprite.Sprite('pic/scull.png', position=scullxy1, opacity=255, scale=1)
        self.add(self.scull1)
        self.list_scull.append(self.scull1)
        self.scull2 = cocos.sprite.Sprite('pic/scull.png', position=scullxy2, opacity=255, scale=1)
        self.add(self.scull2)
        self.list_scull.append(self.scull2)
        self.scull3 = cocos.sprite.Sprite('pic/scull.png', position=scullxy3, opacity=255, scale=1)
        self.add(self.scull3)
        self.list_scull.append(self.scull3)
        self.frog = cocos.sprite.Sprite('pic/frog.png', position=frogxy, opacity=255, scale=1/2)
        self.add(self.frog)
        self.list_frog.append(self.frog)
        black = cocos.sprite.Sprite('pic/cell.png', position=frogxy, opacity=255,color=(0,0,0))
        self.Cell_layer.add(black)
        self.froggo_set.add(frogxy)
        
       
class Level3(cocos.scene.Scene):
    def __init__(self):
        super().__init__()
        Background = cocos.layer.util_layers.ColorLayer(222, 184, 135, 255, width=800, height=600)
        self.add(Background)
        
        self.big_set=set()
        self.big_set_changed=set()
        self.list=[]
        self.list1=[]
        self.chiort_list2=[]
        self.scull_list1=[]
        self.scull_list2=[]
        self.scull_list3=[]
        self.frog_list=[]
        self.k2=0
        self.t2=0
        self.frog_const1=0
        self.frog_const2=0
        scull_coordy_list=[3,5,7]
        scull_coordx_list=[0,2,4]
        
        
        randcoordy1=random.choice(scull_coordy_list)
        scull_coordy_list.remove(randcoordy1)
        randcoordy2=random.choice(scull_coordy_list)
        scull_coordy_list.remove(randcoordy2)
        randcoordy3=random.choice(scull_coordy_list)
        scull_coordy_list.remove(randcoordy3)
        
        randcoordx1=random.choice(scull_coordx_list)
        scull_coordx_list.remove(randcoordx1)
        randcoordx2=random.choice(scull_coordx_list)
        scull_coordx_list.remove(randcoordx2)
        randcoordx3=random.choice(scull_coordx_list)
        scull_coordx_list.remove(randcoordx3)
        
        for i in range(11):
            for j in range(11):
                cell = cocos.sprite.Sprite('pic/cell.png', position=(290+j*21,160+i*21), opacity=255)
                Background.add(cell)
                for k in range(7):
                    if k<=i<11-k and k<=j<11-k:
                        cell.color=(200+k*13,180+k*15,180+k*15)
                if i==5 and j==5:
                    cell.color=COLOR
                    self.big_set_changed.add((cell.x, cell.y))         
                elif i==1 and j>3:
                    self.chiort_list2.append((cell.x-10,cell.y-10))
                self.big_set.add((cell.x, cell.y))
                if i==randcoordy1 and j==randcoordx1:
                    self.scull_list1.append((cell.x,cell.y))
                if i==randcoordy2 and j==randcoordx2:
                    self.scull_list2.append((cell.x,cell.y))
                if i==randcoordy3 and j==randcoordx3:
                    self.scull_list3.append((cell.x,cell.y))
                if i==10:
                    self.frog_list.append((cell.x, cell.y))
                
        self.l = PlayerInput()
        self.add(self.l)
        self.h=HUD()
        self.add(self.h)
        chiortxy2=random.choice(self.chiort_list2)
        scullxy1=random.choice(self.scull_list1)
        scullxy2=random.choice(self.scull_list2)
        scullxy3=random.choice(self.scull_list3)
        frogxy=random.choice(self.frog_list)
        self.a = Act(chiortxy2, scullxy1, scullxy2, scullxy3, frogxy)
        self.add(self.a)
        L=Label(text='the-awesome-dungeon', font_name = 'UA_A_Charming', position=(180,490), font_size=100, align='CENTER', color =(0,0,0,255))
        L1=Label(text='level 3', font_name = 'UA_A_Charming', position=(365,405), font_size=70, align='right', color =(0,0,0,255))

        Background.add(L)
        Background.add(L1)


